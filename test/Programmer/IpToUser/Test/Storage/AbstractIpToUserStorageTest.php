<?php

namespace Programmer\IpToUser\Test\Storage;

use Programmer\IpToUser\Storage\IpToUserStorageInterface;

abstract class AbstractIpToUserStorageTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @return IpToUserStorageInterface
     */
    abstract protected function getStorage();

    public function userIdAndIpProvider()
    {
        return [
            [
                [
                    [0, '127.0.0.1']
                ],
                [
                    [0, ['127.0.0.1']]
                ],
                [
                    ['127.0.0.1', [0]]
                ]
            ],
            [
                [
                    [0, '127.0.0.1'],
                    [1, '127.0.0.1'],
                    [2, '127.0.0.1'],
                    [3, '127.0.0.1'],
                    [4, '127.0.0.1']
                ],
                [
                    [0, ['127.0.0.1']]
                ],
                [
                    ['127.0.0.1', [0, 1, 2, 3, 4]]
                ]
            ],
            [
                [
                    [0, '127.0.0.1'],
                    [0, '127.0.0.2'],
                    [0, '127.0.0.3'],
                    [0, '127.0.0.4'],
                    [0, '127.0.0.5']
                ],
                [
                    [
                        0,
                        ['127.0.0.1', '127.0.0.2', '127.0.0.3', '127.0.0.4', '127.0.0.5']
                    ]
                ],
                [
                    ['127.0.0.1', [0]],
                    ['127.0.0.2', [0]],
                    ['127.0.0.3', [0]],
                    ['127.0.0.4', [0]],
                    ['127.0.0.5', [0]]
                ]
            ],
            [
                [
                    [0, '127.0.0.1'],
                    [1, '127.0.0.1'],
                    [2, '127.0.0.1'],
                    [3, '127.0.0.1'],
                    [4, '127.0.0.1']
                ],
                [
                    [[0, 1, 2, 3, 4], ['127.0.0.1']]
                ],
                [
                    ['127.0.0.1', [0, 1, 2, 3, 4]]
                ]
            ],
            [
                [
                    [0, '127.0.0.1'],
                    [1, '127.0.0.1'],
                    [2, '127.0.0.2'],
                    [3, '127.0.0.2'],
                    [4, '127.0.0.3']
                ],
                [
                    [[0, 1, 2, 3, 4], ['127.0.0.1', '127.0.0.2', '127.0.0.3']]
                ],
                [
                    ['127.0.0.1', [0, 1]],
                    ['127.0.0.2', [2, 3]],
                    ['127.0.0.3', [4]]
                ]
            ],
            [
                [
                    [0, '127.0.0.1'],
                    [0, '127.0.0.2'],
                    [0, '127.0.0.3'],
                    [0, '127.0.0.4'],
                    [0, '127.0.0.5']
                ],
                [
                    [0, ['127.0.0.1', '127.0.0.2', '127.0.0.3', '127.0.0.4', '127.0.0.5']],
                ],
                [
                    [['127.0.0.1', '127.0.0.2', '127.0.0.3', '127.0.0.4', '127.0.0.5'], [0]]
                ]
            ],
            [
                [
                    [0, '127.0.0.1'],
                    [0, '127.0.0.2'],
                    [1, '127.0.0.3'],
                    [1, '127.0.0.4'],
                    [2, '127.0.0.5']
                ],
                [
                    [0, ['127.0.0.1', '127.0.0.2']],
                    [1, ['127.0.0.3', '127.0.0.4']],
                    [2, ['127.0.0.5']]
                ],
                [

                    [['127.0.0.5'], [2]],
                    //
                    [['127.0.0.1', '127.0.0.2'], [0]],
                    [['127.0.0.3', '127.0.0.4'], [1]],
                    [['127.0.0.1', '127.0.0.2', '127.0.0.3', '127.0.0.4'], [0, 1]],
                    [['127.0.0.1', '127.0.0.2', '127.0.0.3', '127.0.0.4', '127.0.0.5'], [0, 1, 2]]
                ]
            ]
        ];
    }

    /**
     * @param array $userIdAndIp
     * @param array $expectedIps
     * @param array $expectedIds
     *
     * @dataProvider userIdAndIpProvider
     */
    public function testStorage($userIdAndIp, $expectedIps, $expectedIds)
    {
        $storage = $this->getStorage();

        foreach ($userIdAndIp as list($userId, $ip)) {
            $storage->addOrUpdateInfo($userId, $ip);
        }

        foreach ($expectedIps as list($userId, $ips)) {
            $this->assertEquals($ips, $storage->getUserIps($userId), '', 0.0, 10, true);
        }

        foreach ($expectedIds as list($ip, $userIds)) {
            $this->assertEquals($userIds, $storage->getUserIdsByIp($ip), '', 0.0, 10, true);
        }
    }
}
