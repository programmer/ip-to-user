<?php

namespace Programmer\IpToUser\Test\Storage;

use Programmer\IpToUser\Storage\IpToUserCompoundStorage;
use Programmer\IpToUser\Storage\IpToUserMemoryStorage;
use Programmer\IpToUser\Storage\IpToUserStorageInterface;

class IpToUserCompoundStorageTest extends AbstractIpToUserStorageTest
{
    public function userIdAndIpProvider()
    {
        return array_merge(
            parent::userIdAndIpProvider(),
            [
                [
                    [
                        [0, '127.0.0.1'],
                        [0, '127.0.0.2'],
                        [0, '2001:0db8:11a3:09d7:1f34:8a2e:07a0:765d'],
                        [0, '3ffe:1900:4545:3:200:f8ff:fe21:67cf']
                    ],
                    [
                        [
                            0, // userId
                            [
                                '127.0.0.1',
                                '127.0.0.2',
                                '2001:0db8:11a3:09d7:1f34:8a2e:07a0:765d',
                                '3ffe:1900:4545:3:200:f8ff:fe21:67cf'
                            ]
                        ]
                    ],
                    [
                        ['127.0.0.1', [0]],
                        ['127.0.0.2', [0]],
                        ['2001:0db8:11a3:09d7:1f34:8a2e:07a0:765d', [0]],
                        ['3ffe:1900:4545:3:200:f8ff:fe21:67cf', [0]]
                    ]
                ],
                [
                    [
                        [0, '127.0.0.1'],
                        [0, '127.0.0.2'],
                        [1, '3ffe:1900:4545:3:200:f8ff:fe21:67cf'],
                        [1, '2001:0db8:11a3:09d7:1f34:8a2e:07a0:765d']
                    ],
                    [
                        [0, ['127.0.0.1', '127.0.0.2']],
                        [1, ['3ffe:1900:4545:3:200:f8ff:fe21:67cf', '2001:0db8:11a3:09d7:1f34:8a2e:07a0:765d']]
                    ],
                    [
                        ['127.0.0.1', [0]],
                        ['127.0.0.2', [0]],
                        [['127.0.0.1','127.0.0.2'], [0]],
                        //
                        ['3ffe:1900:4545:3:200:f8ff:fe21:67cf', [1]],
                        ['2001:0db8:11a3:09d7:1f34:8a2e:07a0:765d', [1]],
                        [['3ffe:1900:4545:3:200:f8ff:fe21:67cf', '2001:0db8:11a3:09d7:1f34:8a2e:07a0:765d'], [1]],
                        //
                        [['127.0.0.1', '3ffe:1900:4545:3:200:f8ff:fe21:67cf'], [0, 1]]
                    ]
                ]
            ]
        );
    }

    /**
     * @return IpToUserStorageInterface
     */
    protected function getStorage()
    {
        return new IpToUserCompoundStorage(
            new IpToUserMemoryStorage(),
            new IpToUserMemoryStorage()
        );
    }
}
