<?php

namespace Programmer\IpToUser\Test\Storage;

use Programmer\IpToUser\Storage\IpToUserMemoryStorage;
use Programmer\IpToUser\Storage\IpToUserStorageInterface;

class IpToUserMemoryStorageTest extends AbstractIpToUserStorageTest
{
    /**
     * @return IpToUserStorageInterface
     */
    protected function getStorage()
    {
        return new IpToUserMemoryStorage();
    }
}
