<?php

namespace Programmer\IpToUser\Test;

use Programmer\IpToUser\IpToUser;
use Programmer\IpToUser\IpToUserInterface;
use Programmer\IpToUser\Storage\IpToUserMemoryStorage;

class IpToUserTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @return IpToUserInterface
     */
    protected function getIpToUser()
    {
        return new IpToUser(new IpToUserMemoryStorage());
    }

    public function deepSearchDataProvider()
    {
        return [
            [
                [ // $userIpData
                    [0, '127.0.0.1'],
                    [1, '127.0.0.1'],
                    [1, '127.0.0.2'],
                    [2, '127.0.0.2'],
                    [3, '127.0.0.2'],
                    [3, '127.0.0.3']
                ],
                [ // deepUserIps [userId, depth, [ip1, ..., ipN]]
                    [0, 0, ['127.0.0.1']],
                    [0, 1, ['127.0.0.1', '127.0.0.2']],
                    [0, 2, ['127.0.0.1', '127.0.0.2', '127.0.0.3']],
                    [0, 3, ['127.0.0.1', '127.0.0.2', '127.0.0.3']],
                ],
                [ // DependentUserIds [userId, depth, [userId1, ..., userIdN]]
                    [0, 0, [0]],
                    [0, 1, [0, 1]],
                    [0, 2, [0, 1, 2, 3]],
                    [0, 3, [0, 1, 2, 3]],
                ]
            ]
        ];
    }

    /**
     * @param array $userIpData
     * @param array $deepUserIps
     * @param array $deepDependentUsers
     *
     * @dataProvider deepSearchDataProvider
     */
    public function testDeepSearch(array $userIpData, array $deepUserIps, array $deepDependentUsers)
    {
        $ipToUser = $this->getIpToUser();

        foreach ($userIpData as list($userIp, $ip)) {
            $ipToUser->addOrUpdateInfo($userIp, $ip);
        }

        foreach ($deepUserIps as list($userId, $depth, $expected)) {
            $this->assertEquals($expected, $ipToUser->getUserIpsDeep($userId, $depth));
        }

        foreach ($deepDependentUsers as list($userId, $depth, $expected)) {
            $this->assertEquals($expected, $ipToUser->getDependentUserIds($userId, $depth));
        }
    }
}
