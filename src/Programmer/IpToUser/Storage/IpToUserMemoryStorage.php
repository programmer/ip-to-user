<?php

namespace Programmer\IpToUser\Storage;

/**
 * Class IpToUserMemoryStorage
 * For test coverage only
 *
 * @package Programmer\IpToUser\Storage
 */
class IpToUserMemoryStorage implements IpToUserStorageInterface
{
    /**
     * @var array
     */
    private $userIdToIp = [];

    /**
     * @var array
     */
    private $IpToUserId = [];

    /**
     * @param int    $userId
     * @param string $ip
     *
     * @return IpToUserStorageInterface
     */
    public function addOrUpdateInfo($userId, $ip)
    {
        $this->userIdToIp[$userId][] = $ip;
        $this->IpToUserId[$ip][] = $userId;

        return $this;
    }

    protected function getUserIdsByIpSingle($ip)
    {
        if (array_key_exists($ip, $this->IpToUserId)) {
            return $this->IpToUserId[$ip];
        }

        return [];
    }

    protected function getUserIdsByIpMultiple(array $ips)
    {
        $result = [];
        foreach ($ips as $ip) {
            $result = array_merge($result, $this->getUserIdsByIpSingle($ip));
        }

        return array_unique($result, SORT_REGULAR);
    }

    /**
     * @param string|string[] $ip
     *
     * @return int[]
     */
    public function getUserIdsByIp($ip)
    {
        return true === is_array($ip) ? $this->getUserIdsByIpMultiple($ip) : $this->getUserIdsByIpSingle($ip);
    }

    protected function getUserIpsSingle($userId)
    {
        if (false === array_key_exists($userId, $this->userIdToIp)) {
            return [];
        }

        return $this->userIdToIp[$userId];
    }

    protected function getUserIpsMultiple(array $userIds)
    {
        $result = [];

        foreach ($userIds as $userId) {
            $result = array_merge($result, $this->getUserIpsSingle($userId));
        }

        return array_unique($result);
    }

    /**
     * @param int|array $userId
     *
     * @return string[]
     */
    public function getUserIps($userId)
    {
        return true === is_array($userId) ? $this->getUserIpsMultiple($userId) : $this->getUserIpsSingle($userId);
    }

    /**
     * @param int $maxAge
     *
     * @return int
     */
    public function deleteOutdatedRecords($maxAge = self::DEFAULT_MAX_AGE)
    {
        return 0;
    }
}
