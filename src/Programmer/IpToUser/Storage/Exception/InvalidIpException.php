<?php
/**
 *
 * @author Yuriy Berest <djua.com@gmail.com>
 */

namespace Programmer\IpToUser\Storage\Exception;

class InvalidIpException extends \Exception
{
}
