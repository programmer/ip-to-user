<?php
/**
 *
 * @author Yuriy Berest <djua.com@gmail.com>
 */

namespace Programmer\IpToUser\Storage;

class IpToUserCompoundStorage implements IpToUserStorageInterface
{
    /**
     * @var IpToUserStorageInterface
     */
    private $ipv4Storage;

    /**
     * @var IpToUserStorageInterface
     */
    private $ipv6Storage;

    /**
     * @param IpToUserStorageInterface $ipv4Storage
     * @param IpToUserStorageInterface $ipv6Storage
     */
    public function __construct(IpToUserStorageInterface $ipv4Storage, IpToUserStorageInterface $ipv6Storage)
    {
        $this->ipv4Storage = $ipv4Storage;
        $this->ipv6Storage = $ipv6Storage;
    }

    /**
     * @return IpToUserStorageInterface
     */
    public function getIpv4Storage()
    {
        return $this->ipv4Storage;
    }

    /**
     * @return IpToUserStorageInterface
     */
    public function getIpv6Storage()
    {
        return $this->ipv6Storage;
    }

    /**
     * @param string $ip
     *
     * @return bool
     */
    protected function isIpv4($ip)
    {
        return false !== filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4);
    }

    /**
     * @param string $ip
     *
     * @return bool
     */
    protected function isIpv6($ip)
    {
        return false !== filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6);
    }

    /**
     * @param string $ip
     *
     * @return IpToUserStorageInterface
     *
     * @throws Exception\InvalidIpException
     */
    protected function getStorageByIp($ip)
    {
        if (true === $this->isIpv4($ip)) {
            return $this->getIpv4Storage();
        }

        if (true === $this->isIpv6($ip)) {
            return $this->getIpv6Storage();
        }

        throw new Exception\InvalidIpException(sprintf('Invalid ip "%s" given.', $ip));
    }

    /**
     * @param int    $userId
     * @param string $ip
     *
     * @return IpToUserStorageInterface
     */
    public function addOrUpdateInfo($userId, $ip)
    {
        return $this->getStorageByIp($ip)->addOrUpdateInfo($userId, $ip);
    }

    protected function getUserIpsByIdSingle($ip)
    {
        return $this->getStorageByIp($ip)->getUserIdsByIp($ip);
    }

    protected function getUserIpsByIpMulti(array $ip)
    {
        $ipv4 = [];
        $ipv6 = [];

        foreach ($ip as $singleIp) {
            if (true === $this->isIpv4($singleIp)) {
                $ipv4[] = $singleIp;
            } elseif (true === $this->isIpv6($singleIp)) {
                $ipv6[] = $singleIp;
            } else {
                throw new Exception\InvalidIpException(sprintf('Invalid ip "%s" given.', $ip));
            }
        }

        $result = [];
        if ([] !== $ipv4) {
            $result = array_merge($result, $this->getIpv4Storage()->getUserIdsByIp($ipv4));
        }

        if ([] !== $ipv6) {
            $result = array_merge($result, $this->getIpv6Storage()->getUserIdsByIp($ipv6));
        }

        return $result;
    }

    /**
     * @param string|string[] $ip
     *
     * @return int[]
     */
    public function getUserIdsByIp($ip)
    {
        return true === is_array($ip) ? $this->getUserIpsByIpMulti($ip) : $this->getUserIpsByIdSingle($ip);
    }

    /**
     * @param int|int[] $userId
     *
     * @return string[]
     */
    public function getUserIps($userId)
    {
        return array_merge($this->getIpv4Storage()->getUserIps($userId), $this->getIpv6Storage()->getUserIps($userId));
    }

    /**
     * @param int $maxAge
     *
     * @return int
     */
    public function deleteOutdatedRecords($maxAge = self::DEFAULT_MAX_AGE)
    {
        $result = $this->getIpv4Storage()->deleteOutdatedRecords($maxAge);
        $result += $this->getIpv6Storage()->deleteOutdatedRecords($maxAge);

        return $result;
    }
}
