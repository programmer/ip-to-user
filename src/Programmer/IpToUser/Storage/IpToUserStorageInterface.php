<?php

namespace Programmer\IpToUser\Storage;

interface IpToUserStorageInterface
{
    /**
     * Max record age in storage (sec)
     */
    const DEFAULT_MAX_AGE = 31536000; // 1 year (365 * 24 * 60 * 60)

    /**
     * @param int    $userId
     * @param string $ip
     *
     * @return IpToUserStorageInterface
     */
    public function addOrUpdateInfo($userId, $ip);

    /**
     * @param string|string[] $ip
     *
     * @return int[]
     */
    public function getUserIdsByIp($ip);

    /**
     * @param int|int[] $userId
     *
     * @return string[]
     */
    public function getUserIps($userId);

    /**
     * @param int $maxAge
     *
     * @return int
     */
    public function deleteOutdatedRecords($maxAge = self::DEFAULT_MAX_AGE);
}
