<?php

namespace Programmer\IpToUser;

interface IpToUserInterface
{
    const DEFAULT_SEARCH_DEPTH = 1;

    /**
     * Создает или обновляет запись о IP адресе пользователя
     *
     * @param int    $userId Id пользователя
     * @param string $ip     Ip входа
     *
     * @return IpToUserInterface
     */
    public function addOrUpdateInfo($userId, $ip);

    /**
     * Получение списка userId по Ip
     *
     * @param string|string[] $ip Ip-адрес
     *
     * @return int[]
     */
    public function getUserIdsByIp($ip);

    /**
     * Получение списка IP адресов для userId
     *
     * @param int|int[] $userId Id пользователя
     *
     * @return string[]
     */
    public function getUserIps($userId);

    /**
     * Получение списка IP адресов для userId c поиском в глубину
     *
     * @param int $userId Id пользователя для которого производится поиск
     * @param int $depth  Глубина поиска
     *
     * @return string[]
     */
    public function getUserIpsDeep($userId, $depth = self::DEFAULT_SEARCH_DEPTH);

    /**
     * Получение аккаунтов на которые заходил пользователель
     *
     * @param int $userId Id пользователя дя которого производится проверка
     * @param int $depth  Глубина поиска
     *
     * @return int[]
     */
    public function getDependentUserIds($userId, $depth = self::DEFAULT_SEARCH_DEPTH);
}
