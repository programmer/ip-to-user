<?php

namespace Programmer\IpToUser;

use Programmer\IpToUser\Storage\IpToUserStorageInterface;

class IpToUser implements IpToUserInterface
{
    /**
     * @var IpToUserStorageInterface
     */
    private $storage;

    /**
     * @param IpToUserStorageInterface $storage
     */
    public function __construct(IpToUserStorageInterface $storage)
    {
        $this->storage = $storage;
    }

    /**
     * @return IpToUserStorageInterface
     */
    protected function getStorage()
    {
        return $this->storage;
    }

    /**
     * {@inheritdoc}
     */
    public function addOrUpdateInfo($userId, $ip)
    {
        $this->getStorage()->addOrUpdateInfo($userId, $ip);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getUserIdsByIp($ip)
    {
        return $this->getStorage()->getUserIdsByIp($ip);
    }

    /**
     * {@inheritdoc}
     */
    public function getUserIps($userId)
    {
        return $this->getStorage()->getUserIps($userId);
    }

    /**
     * @param int $userId
     * @param int $depth
     * @param bool $user
     *
     * @return int[]|string[]
     */
    private function doDeepSearch($userId, $depth, $user)
    {
        $checkedUserIps = [];
        $checkedUserIds = [];

        $userIds = [$userId];

        while (true) {
            if ([] === ($userIdDiff = array_diff($userIds, $checkedUserIds))) {
                break;
            }

            $checkedUserIds = array_merge($checkedUserIds, $userIdDiff);

            if (true === $user && $depth <= 0) {
                break;
            }

            $ips = $this->getUserIps($userIdDiff);
            if ([] === ($ipDiff = array_diff($ips, $checkedUserIps))) {
                break;
            }

            $checkedUserIps = array_merge($checkedUserIps, $ipDiff);

            if (false === $user && $depth <= 0) {
                break;
            }

            $userIds = $this->getUserIdsByIp($ipDiff);

            $depth--;
        }

        return true === $user ? $checkedUserIds : $checkedUserIps;
    }

    /**
     * {@inheritdoc}
     */
    public function getUserIpsDeep($userId, $depth = self::DEFAULT_SEARCH_DEPTH)
    {
        return $this->doDeepSearch($userId, $depth, false);
    }

    /**
     * {@inheritdoc}
     */
    public function getDependentUserIds($userId, $depth = self::DEFAULT_SEARCH_DEPTH)
    {
        return $this->doDeepSearch($userId, $depth, true);
    }
}
